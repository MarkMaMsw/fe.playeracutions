import React from 'react';

import Example from './pages/Example';

import { Route, Switch, BrowserRouter as Router } from 'react-router-dom';

function App() {
  return (
    <Router>
      <Switch>
        {/* Public: 未登录游客，买家，卖家 */}
        <Route path="/login" component={Example} /> {/* 登录页*/}
        <Route path="/signup" component={Example} /> {/* 注册页*/}
        <Route path="/gameindex" component={Example} /> {/* 查看所有游戏列表*/}
        <Route path="/:gamename" component={Example} /> {/* 查看某个游戏的在售账号，物件*/}
        <Route path="/:gamename/:product" component={Example} /> {/* 查看单个产品（账号，物件）的细节*/}
        <Route path="/:seller" component={Example} /> {/* 查看某个seller的主页（seller个人信息，在售产品，其他人的评价...）*/}

        {/* Buyer: 买家 */}
        <Route path="/buyerdashboard" component={Example} /> {/* 买家个人页面： 包括Message, Orders, Account*/}

        {/* Seller: 卖家 */}
        <Route path="/sellerdashboard" component={Example} /> {/* 卖家个人页面： 包括Message, Orders, Account, Products*/}

        {/* Home should be putted last or the Route always porint to Home*/}
        <Route path="/" component={Example} /> {/* Home*/}
      </Switch>
    </Router>
  );
}

export default App;
