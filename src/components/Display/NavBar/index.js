import React from 'react';
import Style from './style.module.scss';
import { withRouter } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGamepad, faUserCircle } from '@fortawesome/free-solid-svg-icons';

function _NavBar() {
  return (
    <header>
      <div className={Style.outerWrapper}>
        <div className={Style.innerWrapper}>
          <div className={Style.logoSection}>
            <a href="#">
              <img
                src="favicon.ico"
                alt="GonnaBuy"
                className={Style.logoImage}
              />
              <span>GonnaBuy</span>
            </a>
          </div>
          <div className={Style.searchSection}>
            <div className={Style.searchSectionInner}>
              <div className={Style.dropdownGroup}>
                <label
                  className={[Style.dropdownToggle, Style.dropdownBtn].join(
                    ' '
                  )}
                >
                  Games<span className={Style.arrow}></span>
                </label>
                <div className={Style.dropdownContent}>
                  <ul>
                    <li className={Style.dropdownItem}>
                      <label className={Style.dropdownLabel} for="dropAccount">
                        <FontAwesomeIcon
                          icon={faUserCircle}
                          className={Style.dropdownIcon}
                        />
                        Account
                      </label>
                      <input
                        type="checkbox"
                        id="dropAccount"
                        className={Style.dropdownInput}
                      />
                    </li>
                  </ul>
                </div>
              </div>
              <span className={Style.searchSectionDivider}></span>
              <div className={Style.searchGroup}>
                <label
                  for="search-input"
                  aria-label="search-input"
                  className={Style.searchInputLabel}
                >
                  <FontAwesomeIcon icon={faGamepad} className={Style.gamepad} />
                </label>

                <input
                  id="search-input"
                  className={Style.searchInput}
                  type="text"
                  aria-label="search"
                  placeholder="Find your games..."
                />
              </div>
            </div>
          </div>
          <div className={Style.accountSection}>
            <div className={Style.accountPanel}>
              <a href="#" className={Style.btnLogin}>
                LOG IN
              </a>
              <a href="#" className={Style.btnSignup}>
                SIGN UP
              </a>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
}

const NavBar = withRouter(_NavBar);

export { NavBar };
